<?php
require __DIR__ . '/vendor/autoload.php';

use src\CarrinhoCompra;
use src\Item;
use src\Pedido;
use src\EmailService;

echo '<h3>Com SRP</h3>';
$pedido = new Pedido();

$item1 = new Item();
$item1->setDescricao('Coca cola Lata');
$item1->setValor(3.99);

$item2 = new Item();
$item2->setDescricao('Coca cola 2L');
$item2->setValor(7.99);


echo '<h3>Pedido sem itens</h3>';
echo '<pre>';
print_r($pedido);
echo '</pre>';

$pedido->getCarrinhoCompra()->adicionarItem($item1);
$pedido->getCarrinhoCompra()->adicionarItem($item2);

echo '<h3>Pedido com itens</h3>';
echo '<pre>';
print_r($pedido);
echo '</pre>';


echo '<h3>Itens do carrinho</h3>';
echo '<pre>';
print_r($pedido->getCarrinhoCompra()->getItens());
echo '</pre>';

echo '<h3>Valor do pedido</h3>';
$total = 0;
foreach($pedido->getCarrinhoCompra()->getItens() as $key => $item){
    $total += $item->getValor();
}
echo $total;


echo '<h3>O carrinho está válido</h3>';
echo '<pre>';
var_dump($pedido->getCarrinhoCompra()->validarCarrinho());
echo '</pre>';

echo '<h3>Status do pedido</h3>';
echo '<pre>';
var_dump($pedido->getStatus());
echo '</pre>';

echo '<h3>Confirmar o pedido</h3>';
echo '<pre>';
var_dump($pedido->confirmar());
echo '</pre>';

echo '<h3>Email</h3>';
if($pedido->getStatus() == 'Confirmado'){
    echo EmailService::dispararEmail();
}
