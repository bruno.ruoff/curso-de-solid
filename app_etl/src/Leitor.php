<?php

namespace src;

use src\Arquivo;

class Leitor
{
    private $diretorio;
    private $arquivo;

    public function getDiretorio(): string
    {
        return $this->diretorio;
    }

    public function getArquivo(): string
    {
        return $this->arquivo;
    }

    public function setArquivo(string $arquivo): void
    {
        $this->arquivo = $arquivo;
    }

    public function setDiretorio(string $diretorio): void
    {
        $this->diretorio = $diretorio;
    }

    public function lerArquivo(): array
    {
        $caminho = $this->getDiretorio() . DIRECTORY_SEPARATOR . $this->getArquivo();

        $arquivo = new Arquivo();

        $ext = explode('.', $caminho);

        if ($ext[1] == 'csv') {
            $arquivo->lerArquivoCSV($caminho);
        } elseif ($ext[1] == 'txt') {
            $arquivo->lerArquivoTXT($caminho);
        }
        return $arquivo->getDados();
    }
}