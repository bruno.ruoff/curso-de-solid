<?php

require __DIR__ . "/vendor/autoload.php";

use src\Leitor;

echo '<pre>';

/**
 *  TXT
 */
$leitor = new Leitor();
$leitor->setDiretorio(__DIR__ . '/arquivos');
$leitor->setArquivo('dados.txt');
$arrTxt = $leitor->lerArquivo();

/**
 *  CSV
 */

$leitorCSV = new Leitor();
$leitorCSV->setDiretorio(__DIR__ . '/arquivos');
$leitorCSV->setArquivo('dados.csv');
$arrCSV = $leitor->lerArquivo();

/**
 * MERGE
 */

echo '<pre>';
print_r(array_merge($arrTxt, $arrCSV));

